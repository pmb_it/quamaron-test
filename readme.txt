Вимоги: 
- ansible >= 2.9.6
- bash >= 4 (для виконання one-liner нижче)

Використання:
1. Повний редеплой всіх проектів в prod і stage оточеннях: 
ansible-playbook -i hosts main.yml

2. Редеплой всіх проектів в одному оточенні (prod|stage):
ansible-playbook -i hosts main.yml -l stage

3. Редеплой списка існуючих проектів в оточенні prod|stage
3.1. Переконатись, що проект(и) вже додано до group_vars/<оточення>.yml
3.2. Створити файл зі списком необхідних проектів (див. examples/proj_list.yml)

ansible-playbook -i hosts main.yml --extra-vars "@proj_list.yml" -l stage


4. Редеплой cписка проектів для обох оточень:
4.1. Переконатись, що проект(и) вже додано до group_vars/<оточення>.yml
4.2. Створити файл зі списком необхідних проектів (див. examples/proj_list.yml)
Приклад: 
for APP_ENV in prod stage;do ansible-playbook -i hosts main.yml --extra-vars "@proj_list.yml" -l $APP_ENV;done


5. Додавання нових проек:
5.1. Внести в секцію "projects" в файлах змінних оточення (group_vars/prod.yml та/або group_vars/stage.yml) запис виду
  - name: <project_name>
    group: <project_group>

Приклад:
projects:
# ---- новий запис ------
  - name: awesome
    group: one
# ---- новий запис ------

5.2. Звернути увагу на пробіли, це YAML.

5.3. Запустити деплой нового проекту (див. п.1-4).
